# React-Redux Learning

- React is a library used to build user interfaces.

## Redux

- Redux is a library for managing state in a predictable way in Javascript applications.

### Core Concepts

- - Store - Holds the state of your application.
- - Action - Describes the changes in the state of the application.
- - Reducer - Which actually carries out the state transition depending on the action.

### Three Principles

## Credits: Code Evolution
